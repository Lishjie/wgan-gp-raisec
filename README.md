Improved Training of Wasserstein GANs
=====================================

Code for reproducing experiments in ["Improved Training of Wasserstein GANs"](https://arxiv.org/abs/1704.00028).</br>

利用WGAN进行问卷数据集的对抗生成实验


## Prerequisites

- Python, NumPy, TensorFlow, SciPy, Matplotlib
- A recent NVIDIA GPU

## Models

2020.05.14更新：删除掉了一些无用的文件，保留的文件结构说明如下

`others`文件夹
 - 存放着对比方法的代码，可以根据riasec_result8200.png文件查看具体方法的效果
 - 需要注意一下图中的MD方法可能为代码中的ma_acc_49.py方法

`save`文件夹
 - 约定整个工程的所有输出文件均存入该文件夹下面
 - 已经将已训练好的模型、采样文件和log信息删掉（为了优化项目的大小）
 - `acc_result` 文件夹内存放的是所有方法的测试精度
 - `real_data_test.txt` 训练集  `real_data_train.txt` 测试集
 - `real_data.txt` 所有数据集
 - `test_negative_random.txt` 负样本

`tflib`文件夹
 - 工程需要用到的库函数文件夹，里面存放了画图函数和基础模型函数（读源代码的时候会体会到用途）

剩余文件
 - `gan_language_raisec.py`：论文所提出的gan网络
 - `evaluate_raisec_D-out.py`：利用训练好的gan模型生成造假样本信息
 - `language_helpers.py`：训练数据的处理库函数
 - `ma_acc_49.py`：ma方法
 - `ma_acc.py`：ma方法
 - `riasec_D_plot.py`：得出论文提出的gan方法的测试精度
 - `riasec_plot.py`：得出所有方法的精度对比图
 - `test_negative_random.txt`：负样本
 - `riasec_result8200.png`：精度对比图

## Hyperparameter

big5心理问卷数据集设置

- `BATCH_SIZE`: 64
- `ITERS`: 10000
- `SEQ_LEN`: 49
- `DIM`: 20
- `CRITIC_ITERS`: 10
- `LAMBDA`: 10
- `MAX_N_EXAMPLES`: 7187
- `FILE_PATH`: './save/big5/'
- `filter_size`: 10 per 10 questions as a group

riasec心理问卷数据集设置

- `BATCH_SIZE`: 64
- `ITERS`: 10000
- `SEQ_LEN`: 49
- `DIM`: 20
- `CRITIC_ITERS`: 10
- `LAMBDA`: 10
- `MAX_N_EXAMPLES`: 7187
- `FILE_PATH`: './save/big5/'
- `filter_size`: 10 per 10 questions as a group
